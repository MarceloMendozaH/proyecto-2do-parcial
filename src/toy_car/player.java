/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toy_car;

import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class player extends Application {
  private static final int      KEYBOARD_MOVEMENT_DELTA = 5;
  private double CURRENT_ANGLE = 0 ;
  private boolean left_pressed = false;
  private boolean right_pressed = false ;
  private double NORMA = 10 ;
  public static void main(String[] args) { launch(args); }
  @Override public void start(Stage stage) throws Exception {
    final Rectangle player = createRectangle();
    final Group group = new Group(player);

    
    final Scene scene = new Scene(group, 600, 400, Color.CORNSILK);
    moveCircleOnKeyPress(scene, player);
     player.setTranslateY(200);
     player.setTranslateX(300);
    
    stage.setScene(scene);
    stage.show();
  }


  private Rectangle createRectangle() {
    final Rectangle player = new Rectangle(200, 150, Color.BLUEVIOLET);
    player.setOpacity(0.7);
    return player;
  }



  private void moveCircleOnKeyPress(Scene scene, final Rectangle player) {
    scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
      @Override public void handle(KeyEvent event) {
        double x=0;
        double y=0;
        switch (event.getCode()) {
          case UP:    
              if (left_pressed==false && right_pressed ){
                player.setRotate(player.getRotate() + 20);  
                CURRENT_ANGLE +=20;
              }
              if(left_pressed && right_pressed==false){
                  player.setRotate(player.getRotate() - 20);
                  CURRENT_ANGLE-=20;
              }
              x = Math.cos(CURRENT_ANGLE)*NORMA;
             y = Math.sin(CURRENT_ANGLE)*NORMA;
              player.setTranslateY(player.getTranslateY() - y);
              player.setTranslateX(player.getTranslateX() - x);
              
               break;
          
         // case RIGHT: player.setTranslateX(player.getTranslateX() + KEYBOARD_MOVEMENT_DELTA);break;
          case RIGHT: right_pressed = true;break;
          case DOWN:  player.setTranslateY(player.getTranslateY() + KEYBOARD_MOVEMENT_DELTA); break;
          case LEFT: left_pressed = true;break;
        }
      }
    });
  }

}