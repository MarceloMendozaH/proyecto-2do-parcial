package toy_car;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class GameMain  {

    static Random random = new Random();
    
  private static final int      KEYBOARD_MOVEMENT_DELTA = 5;
  private double CURRENT_ANGLE = 0 ;
  private boolean left_pressed = false;
  private boolean right_pressed = false ;
  private boolean up = false;
  private boolean down = false;
 
    
    
    Layer playfield;
    Vehicle vehicle;
    AnimationTimer gameLoop;
    Vector2D mouseLocation = new Vector2D( 0, 0);

    public Scene scene;
    public BorderPane root;
    

   
     public GameMain() {

        // create containers
        BorderPane root = new BorderPane();

        // playfield for our Sprites
        playfield = new Layer( Settings.SCENE_WIDTH, Settings.SCENE_HEIGHT);

        // entire game as layers
        Pane layerPane = new Pane();

        layerPane.getChildren().addAll(playfield);

        root.setCenter(layerPane);

        scene = new Scene(root, Settings.SCENE_WIDTH, Settings.SCENE_HEIGHT);


        // add content
        prepareGame();

        // crear input
        player_input();

        // run animation loop
        startGame();


    }

    private void prepareGame() {

            addVehicles();
        
      

    }

    private void startGame() {

        // start game
        gameLoop = new AnimationTimer() {

            @Override
            public void handle(long now) {


                // seek attractor location, apply force to get towards it
               

                // move sprite
                vehicle.display();
                if(up && left_pressed){
                    vehicle.rotation_input(left_pressed,right_pressed);
                    vehicle.move();
                    
                }
                if(up){
                   vehicle.move();
                }
                if(up && right_pressed){
                    vehicle.rotation_input(left_pressed,right_pressed);
                    vehicle.move();
                    
                }   

               
                
            }
        };

        gameLoop.start();

    }

    /**
     * Add single vehicle to list of vehicles and to the playfield
     */
    private void addVehicles() {

        Layer layer = playfield;

        // random location
        double x = random.nextDouble() * layer.getWidth();
        double y = random.nextDouble() * layer.getHeight();

        // dimensions
        double width = 50;
        double height = width / 2.0;

        // create vehicle data
        Vector2D location = new Vector2D( x,y);
        Vector2D velocity = new Vector2D( 0,0);
        Vector2D acceleration = new Vector2D( 1,1);

        // create sprite and add to layer
       vehicle = new Vehicle( layer, location, velocity, acceleration, width, height);


    }


   
    public void player_input(){
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
                             @Override public void handle(KeyEvent event) {
                             
                                 if(event.getCode()==  KeyCode.UP ){
                                   
                                   up = true;
                                 }
                                 if(event.getCode()==  KeyCode.LEFT ){
                                   left_pressed = true;
                                 }
                                 if(event.getCode()==  KeyCode.RIGHT){
                                   right_pressed = true;
                                 }
        
      }
    });
        
        
        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
                             @Override public void handle(KeyEvent event) {
       
                                switch (event.getCode()) {
                                case UP: up= false;
                                 break;
          
         // case RIGHT: player.setTranslateX(player.getTranslateX() + KEYBOARD_MOVEMENT_DELTA);break;
                                 case RIGHT: right_pressed = false;break;
                                 case DOWN:  down = false; break;
                                 case LEFT: left_pressed = false;break;
        }
      }
    });
    }

    public Scene getScene() {
        return scene;
    }

    public BorderPane getRoot() {
        return root;
    }

}