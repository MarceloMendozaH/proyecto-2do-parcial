/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import toy_car.GameMain;

/**
 *
 * @author user
 */
public class menu_principal extends Application {
       
    @Override
    public void start(Stage primaryStage) throws Exception {
    VBox root=new VBox();
            Scene scene=new Scene(root,300,150,Color.BLUE);
            HBox h1= new HBox();
            Button nuevo_juego= new Button("Nuevo Juego");
            nuevo_juego.setStyle("-fx-background-color: MediumSeaGreen");           
            h1.getChildren().add(nuevo_juego);
            h1.setAlignment(Pos.CENTER);
            
            Button lista_ganadores= new Button("Lista Ganadores");
            lista_ganadores.setStyle("-fx-background-color: MediumSeaGreen");
            HBox h2= new HBox();                      
            h2.getChildren().add(lista_ganadores);
            h2.setAlignment(Pos.CENTER);
            
            Button salir= new Button("Salir");
            salir.setStyle("-fx-background-color:MediumSeaGreen");         
            HBox h3= new HBox();                      
            h3.getChildren().add(salir);
            h3.setAlignment(Pos.CENTER);
           
            nuevo_juego.setPrefWidth(150);
            lista_ganadores.setPrefWidth(150);
            salir.setPrefWidth(150);
            root.getChildren().addAll(h1,h2,h3);
            root.setSpacing(20);
           
          
            root.setStyle("-fx-background-color: GREY");
            primaryStage.setTitle("Parking Game");
            primaryStage.setScene(scene);
            primaryStage.show();
           
            VBox root2=new VBox();
            Scene scene2=new Scene(root2,300,150,Color.BLUE);
            Label name= new Label("Nombre");
            TextField nombre= new TextField();
            HBox h4= new HBox();
            h4.getChildren().addAll(name,nombre);
            h4.setSpacing(10);
            
            HBox h5= new HBox();
            Button aceptar= new Button("Aceptar");
            aceptar.setStyle("-fx-background-color: MediumSeaGreen");
            aceptar.setPrefWidth(150);
            h5.getChildren().add(aceptar);
            h5.setAlignment(Pos.CENTER);
            root2.getChildren().addAll(h4,h5);
            root2.setSpacing(20);
            root2.setStyle("-fx-background-color: GREY;");
            nuevo_juego.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
              primaryStage.setScene(scene2);
              primaryStage.show();
              primaryStage.setTitle("Jugador");
            }
            });
            
            aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
            Scene escena_datos = new GameMain().getScene();
            Stage st = new Stage();
            st.setScene(escena_datos);
            st.show();
            }
            });
            
            salir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
             VBox root3=new VBox();
             Scene scene3=new Scene(root3,300,100,Color.BLUE);
             Label pregunta= new Label("   ¿Desea salir del juego?");
             HBox h5= new HBox();
             h5.getChildren().add(pregunta);
             
             HBox h6= new HBox();
             Button si = new Button("SI");
             si.setStyle("-fx-background-color: MediumSeaGreen");
             Button no = new Button("NO");
             no.setStyle("-fx-background-color: MediumSeaGreen");
             h6.getChildren().addAll(si,no);
             h6.setAlignment(Pos.CENTER);
             h6.setSpacing(10);
             root3.getChildren().addAll(h5,h6);
             root3.setSpacing(20); 
             root3.setStyle("-fx-background-color: GREY;");
             
              
            primaryStage.setScene(scene3);
            primaryStage.show();
            primaryStage.setTitle("Confirmación");
            }
        });
            
            lista_ganadores.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
            VBox root4=new VBox();
            Scene scene4=new Scene(root4,300,150,Color.BLUE);
             
            HBox h7= new HBox();
            Button aceptarr= new Button("Aceptar");
            aceptarr.setStyle("-fx-background-color: MediumSeaGreen");
            h7.getChildren().add(aceptarr);
            h7.setAlignment(Pos.CENTER);
            TextArea text= new TextArea();
            text.appendText("Nombre,  Tiempo,  Puntaje,  Fecha");
            try{
                File  archivo=new File("historial.txt");
                FileReader  fr= new FileReader(archivo);
                BufferedReader bf= new BufferedReader(fr);
                String linea;      
                while((linea=bf.readLine())!=null){
                    text.appendText(linea);
                    fr.close(); 
                }           
            }
            catch(IOException fil){
                 System.out.println("Archivo no encontrado");
            } 
            root4.getChildren().addAll(text,h7);
            root4.setSpacing(20); 
            root4.setStyle("-fx-background-color: GREY;");
             
              
            primaryStage.setScene(scene4);
            primaryStage.show();
            primaryStage.setTitle("Lista de Ganadores");
              
            }
       
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
    
 
